<?php
namespace App\Repositories\Eloquent;

use App\Models\Log;
use App\Repositories\BaseRepository;
use \Illuminate\Support\Facades\DB;

class LogsRepository extends BaseRepository
{
    protected $modelClass = Log::class;

    private function getCountByColumnName($columnName, $asName = null)
    {
        if (is_null($asName)) {
            $asName = $columnName;
        }

        $query = $this->query()
            ->select(DB::raw("{$columnName} AS {$asName}, COUNT(*) AS total"))
            ->groupBy($columnName)
            ->orderByDesc('total');

        return $this->executeQuery($query);
    }

    public function getByService()
    {
        return $this->getCountByColumnName('service_name', '`Service name`');
    }

    public function getByConsumer()
    {
        return $this->getCountByColumnName('consumer_uuid', '`Consumer uniq id`');
    }

    public function getAverageMetrics()
    {
        $query = $this->query()
            ->select(DB::raw("service_name AS `Service name`, ROUND(AVG(proxy_time), 2) AS `Tempo médio Proxy`, ROUND(AVG(kong_time), 2) AS `Tempo médio Kong`, ROUND(AVG(request_time), 2) AS `Tempo médio Request`"))
            ->groupBy('service_name');

        return $this->executeQuery($query);
    }
}
