<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Base para todos os repositórios
 */
abstract class BaseRepository
{
    protected $modelClass;

    protected function getInstance(): Model
    {
        return app($this->modelClass);
    }

    protected function query()
    {
        return $this->getInstance()->query();
    }

    protected function executeQuery($query = null)
    {
        if (is_null($query)) {
            $query = $this->query();
        }

        return $query->get();
    }

    public function getAll($limit = null, $offset = 0)
    {
        $query = $this->query();

        if (!is_null($limit)) {
            $query->limit($limit)
                ->offset($offset);
        }

        return $this->executeQuery($query);
    }

    public function insert(array $rows)
    {
        $this->getInstance()->insert($rows);
    }
}
