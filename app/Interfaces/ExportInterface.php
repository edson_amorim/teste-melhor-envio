<?php
namespace App\Interfaces;

interface ExportInterface
{
    /**
     * Processa os dados ($options) e retorna o nome do arquivo exportado
     *
     * @param array $options
     * @return string output filename
     */
    public function build(array $options): string;
}
