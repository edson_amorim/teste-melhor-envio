<?php
namespace App\Interfaces;

/**
 * interface para adapters, prevê o método convert, utilizado como padrão para qualquer adaptador,
 * que deverá retornar um array
 */
interface AdapterInterface
{
    public function convert($argument): array;
}
