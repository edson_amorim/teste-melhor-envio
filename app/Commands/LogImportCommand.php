<?php

namespace App\Commands;

use App\Adapters\FileRowToLogAdapter;
use App\Repositories\Eloquent\LogsRepository;
use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;

class LogImportCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'log:import
                            {filename : O caminho absoluto para o arquivo de log a ser importado}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');

        if (!$filename) {
            return $this->error('Filename não informado!');
        }

        $microtimeInicio = microtime(true);

        $this->line('Armazenando logs no banco de dados');

        $this->importRows($this->getFile($filename));

        $this->info('Logs importados');
        $this->info('Tempo de execução: ' . (round(microtime(true) - $microtimeInicio, 1)) . 's');
    }

    private function getFile($filename)
    {
        $this->line("Obtendo arquivo {$filename}");

        $contents = File::get($filename);

        return explode("\n", $contents);
    }

    private function importRows(array $rows)
    {
        $repository = new LogsRepository();
        $adapter    = new FileRowToLogAdapter();
        $logs       = [];

        foreach ($rows as $index => $row) {
            if (!$row) {
                continue;
            }

            $logArray = $adapter->convert($row);

            if (!$logArray) {
                $this->line('Linha "' . ($index + 1) . '" no formato inválido. Ignorando importação');
                continue;
            }

            $logs[] = $logArray;
        }

        while ($toSave = array_splice($logs, 0, 5000)) {
            $repository->insert($toSave);
        }
    }
}
