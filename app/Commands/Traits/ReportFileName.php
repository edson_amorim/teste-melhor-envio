<?php
namespace App\Commands\Traits;

trait ReportFileName
{
    public function getOutputFileName($file): string
    {
        $outputFile = $this->argument('outputFile');
        if (!$outputFile) {
            $outputFile = implode(DIRECTORY_SEPARATOR, [base_path(), 'export', $file]);
        }

        return $outputFile;
    }
}
