<?php

namespace App\Commands;

use App\Adapters\CollectionToCsvModelAdapter;
use App\Commands\Traits\ReportFileName;
use App\Repositories\Eloquent\LogsRepository;
use App\Services\ExportService;
use LaravelZero\Framework\Commands\Command;

class ReportAverageCommand extends Command
{
    use ReportFileName;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'report:average {outputFile?}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Obtém o relatório de tempo médio de execução por serviço';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $microtimeInicio = microtime(true);

        $this->line('Buscando tempo médio de execução por serviço');

        $fileName = $this->process();

        $this->info("Relatório exportado para o arquivo {$fileName}");
        $this->info('Tempo de execução: ' . (round(microtime(true) - $microtimeInicio, 1)) . 's');
    }

    private function process(): string
    {
        $repository = new LogsRepository();
        $adapter    = new CollectionToCsvModelAdapter();

        $exportData                   = $adapter->convert($repository->getAverageMetrics());
        $exportData['outputFileName'] = $this->getOutputFileName('average.csv');

        $exportFactory = new ExportService();
        return $exportFactory->export('csv', $exportData);
    }
}
