<?php

namespace App\Commands;

use Illuminate\Foundation\Console\ModelMakeCommand;

class ModelMakeCustomCommand extends ModelMakeCommand
{
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\Models';
    }
}
