<?php
namespace App\Exceptions;

use Exception;

/**
 * exception para falhas de instância de classes
 */
class InstanceException extends Exception
{}
