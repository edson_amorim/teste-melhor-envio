<?php
namespace App\Exceptions;

use Exception;

/**
 * exception para casos onde o arquivo não foi salvo
 */
class FileSaveException extends Exception
{}
