<?php
namespace App\Services;

use App\Exceptions\InstanceException;
use App\Interfaces\ExportInterface;
use InvalidArgumentException;

/**
 * Factory para os serviços de exportação
 */
class ExportService
{
    /**
     * Instancia dinamicamente um service pelo tipo de arquivo a ser exportado ($typeName)
     *
     * envia $options para ser processado pela instância do serviço de exportação
     *
     * @param string $typeName
     * @param array $options
     * @return string caminho completo do arquivo salvo
     */
    public function export(string $typeName, array $options): string
    {
        $typeName  = ucfirst(strtolower($typeName));
        $className = "App\\Services\\Export\\{$typeName}Export";

        if (!class_exists($className)) {
            throw new InvalidArgumentException("Classe '{$className}' não encontrada!");
        }

        $instance = new $className;

        if (!$instance instanceof ExportInterface) {
            throw new InstanceException("{$className} não implementa a interface App\Interfaces\ExportInterface");
        }

        return $instance->build($options);
    }
}
