<?php
namespace App\Services\Export;

use App\Exceptions\FileSaveException;

/**
 * Base para todos os tipos de exportação
 */
class BaseExport
{

    /**
     * Recebe o conteúdo do arquivo ($fileData) e o destino onde será salvo ($fileName)
     *
     * Após salvo, retorna o caminho completo do arquivo salvo
     *
     * @param string $fileData
     * @param string $fileName
     * @return string nome do arquivo
     */
    protected function writeFile(string $fileData, string $fileName): string
    {
        $fileDirectory = dirname($fileName);

        if (!is_dir($fileDirectory)) {
            mkdir($fileDirectory, 0775, true);
        }

        if (!@file_put_contents($fileName, $fileData)) {
            throw new FileSaveException("Não foi possível salvar o arquivo no diretório. Verifique se o diretório existe e se tem permissão de escrita");
        }

        return $fileName;
    }
}
