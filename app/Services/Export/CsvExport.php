<?php
namespace App\Services\Export;

use App\Interfaces\ExportInterface;
use League\Csv\Writer;

class CsvExport extends BaseExport implements ExportInterface
{
    public function build(array $options): string
    {
        $csvString = $this->createCsv($options);
        return $this->writeFile($csvString, $options['outputFileName']);
    }

    /**
     * Cria o arquivo CSV a partir do array de $options com a classe League\Csv\Writer
     *
     * @param array $options
     * @return string conteúdo do CSV
     */
    private function createCsv(array $options): string
    {
        $csv = Writer::createFromString();

        $csv->insertOne($options['header']);
        $csv->insertAll($options['records']);

        return $csv->toString();
    }
}
