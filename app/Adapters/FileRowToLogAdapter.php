<?php
namespace App\Adapters;

use App\Interfaces\AdapterInterface;

class FileRowToLogAdapter implements AdapterInterface
{

    /**
     * converte uma linha como string json no formato do arquivo de log para a estrutura dos campos de um model \App\Models\Log
     *
     * @param string $row
     * @return array
     */
    public function convert($row): array
    {
        if (!$decoded = json_decode($row)) {
            return [];
        }

        return [
            'service_name'  => $decoded->service->name,
            'consumer_ip'   => $decoded->client_ip,
            'consumer_uuid' => $decoded->authenticated_entity->consumer_id->uuid,
            'proxy_time'    => $decoded->latencies->proxy,
            'kong_time'     => $decoded->latencies->kong,
            'request_time'  => $decoded->latencies->request,
        ];
    }
}
