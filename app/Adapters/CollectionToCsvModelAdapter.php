<?php
namespace App\Adapters;

use App\Interfaces\AdapterInterface;

class CollectionToCsvModelAdapter implements AdapterInterface
{
    /**
     * Converte uma collection de um ou mais registros para o formato do \App\Services\Export\CsvExport
     *
     * Caso seja passada uma collection sem registros dispara uma exceção \InvalidArgumentException
     *
     * @param \Illuminate\Database\Eloquent\Collection $collection
     * @return array
     */
    public function convert($collection): array
    {
        if (!count($collection)) {
            throw new \InvalidArgumentException("Collection sem valores");
        }

        $return = [
            "header"  => array_keys($collection[0]->getAttributes()),
            "records" => array_values($collection->toArray()),
        ];

        return $return;
    }
}
