<?php

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LogImportCommandTest extends TestCase
{
    use DatabaseTransactions;

    public function testLogImportSuccessfulCommand()
    {
        $this->artisan('log:import ' . config('test.assetsDir') . DIRECTORY_SEPARATOR . 'logs.txt')
            ->expectsOutput('Logs importados')
            ->assertExitCode(0);
    }

    public function testLogImportFileNotFoundCommand()
    {
        $this->expectException(FileNotFoundException::class);
        $this->artisan('log:import inexistentFile.txt');
    }
}
