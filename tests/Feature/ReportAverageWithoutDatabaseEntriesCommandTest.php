<?php

use Tests\TestCase;

class ReportAverageWithoutDatabaseEntriesCommandTest extends TestCase
{
    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     *
     * @return void
     */
    public function testInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Collection sem valores');

        $outputFilePath = base_path('export' . DIRECTORY_SEPARATOR . 'average.csv');
        $this->artisan('report:average ' . $outputFilePath);
    }
}
