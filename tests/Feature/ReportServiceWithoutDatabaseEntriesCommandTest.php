<?php

use Tests\TestCase;

class ReportServiceWithoutDatabaseEntriesCommandTest extends TestCase
{
    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     *
     * @return void
     */
    public function testInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Collection sem valores');

        $outputFilePath = base_path('export' . DIRECTORY_SEPARATOR . 'service.csv');
        $this->artisan('report:service ' . $outputFilePath);
    }
}
