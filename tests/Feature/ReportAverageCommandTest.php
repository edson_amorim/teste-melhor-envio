<?php

use App\Exceptions\FileSaveException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\SetUpLogImportCommand;

class ReportAverageCommandTest extends TestCase
{
    use DatabaseTransactions;
    use SetUpLogImportCommand;

    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     *
     * @return void
     */
    public function testReportAverageSuccessfulCommand()
    {
        $outputFilePath = base_path('export' . DIRECTORY_SEPARATOR . 'average.csv');

        $this->artisan('report:average ' . $outputFilePath)
            ->expectsOutput('Relatório exportado para o arquivo ' . $outputFilePath)
            ->assertExitCode(0);
    }

    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     */
    public function testReportAverageWriteFileForbiddenLocationCommand()
    {
        $this->expectException(FileSaveException::class);
        $this->artisan('report:average /teste.csv');
    }
}
