<?php

use App\Exceptions\FileSaveException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\SetUpLogImportCommand;

class ReportServiceCommandTest extends TestCase
{
    use DatabaseTransactions;
    use SetUpLogImportCommand;

    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     *
     * @return void
     */
    public function testReportServiceSuccessfulCommand()
    {
        $outputFilePath = base_path('export' . DIRECTORY_SEPARATOR . 'service.csv');

        $this->artisan('report:service ' . $outputFilePath)
            ->expectsOutput('Relatório exportado para o arquivo ' . $outputFilePath)
            ->assertExitCode(0);
    }

    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     */
    public function testReportServiceWriteFileForbiddenLocationCommand()
    {
        $this->expectException(FileSaveException::class);
        $this->artisan('report:service /teste.csv');
    }
}
