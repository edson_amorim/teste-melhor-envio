<?php

use App\Exceptions\FileSaveException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\SetUpLogImportCommand;

class ReportConsumerCommandTest extends TestCase
{
    use DatabaseTransactions;
    use SetUpLogImportCommand;

    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     *
     * @return void
     */
    public function testReportConsumerSuccessfulCommand()
    {
        $outputFilePath = base_path('export' . DIRECTORY_SEPARATOR . 'consumer.csv');

        $this->artisan('report:consumer ' . $outputFilePath)
            ->expectsOutput('Relatório exportado para o arquivo ' . $outputFilePath)
            ->assertExitCode(0);
    }

    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     */
    public function testReportConsumerWriteFileForbiddenLocationCommand()
    {
        $this->expectException(FileSaveException::class);
        $this->artisan('report:consumer /teste.csv');
    }
}
