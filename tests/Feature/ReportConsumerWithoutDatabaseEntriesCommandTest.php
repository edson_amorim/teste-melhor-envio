<?php

use Tests\TestCase;

class ReportConsumerWithoutDatabaseEntriesCommandTest extends TestCase
{
    /**
     * @Depends LogImportCommandTest::testLogImportSuccessfulCommand
     *
     * @return void
     */
    public function testInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Collection sem valores');

        $outputFilePath = base_path('export' . DIRECTORY_SEPARATOR . 'consumer.csv');
        $this->artisan('report:consumer ' . $outputFilePath);
    }
}
