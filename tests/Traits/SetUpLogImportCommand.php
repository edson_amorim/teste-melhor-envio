<?php

namespace Tests\Traits;

trait SetUpLogImportCommand
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('log:import ' . config('test.assetsDir') . DIRECTORY_SEPARATOR . 'logs.txt');
    }
}
