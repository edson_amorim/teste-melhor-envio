<?php

use App\Exceptions\FileSaveException;
use App\Services\ExportService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\SetUpLogImportCommand;

class ExportCsvTest extends TestCase
{
    use DatabaseTransactions;
    use SetUpLogImportCommand;

    public function testExportCsvSuccessful()
    {
        $factory  = new ExportService();
        $fileName = $factory->export('csv', $this->exportFakeData(config('test.assetsDir') . DIRECTORY_SEPARATOR . 'teste.csv'));

        $this->assertFileExists($fileName);
        @unlink($fileName);

    }

    public function testExportCsvFileForbiddenLocation()
    {
        $this->expectException(FileSaveException::class);

        $factory = new ExportService();
        $factory->export('csv', $this->exportFakeData(DIRECTORY_SEPARATOR . 'teste.csv'));
    }

    protected function exportFakeData($outputFileName): array
    {
        return [
            'header'         => ['test'],
            'records'        => [[1], [2]],
            'outputFileName' => $outputFileName,
        ];
    }
}
