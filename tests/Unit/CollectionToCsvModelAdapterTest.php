<?php

use App\Adapters\CollectionToCsvModelAdapter;
use App\Repositories\Eloquent\LogsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\SetUpLogImportCommand;

class CollectionToCsvModelAdapterTest extends TestCase
{
    use DatabaseTransactions;
    use SetUpLogImportCommand;

    public function testConvertSuccessful()
    {
        $repository = new LogsRepository();
        $collection = $repository->getAll(2);

        $adapter   = new CollectionToCsvModelAdapter();
        $converted = $adapter->convert($collection);

        $this->assertArrayHasKey('header', $converted);
        $this->assertArrayHasKey('records', $converted);
    }
}
