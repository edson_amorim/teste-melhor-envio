# Teste técnico - Edson Amorim Ferreira Junior


## Informações gerais
Nome do app: `teste-melhor-envio`

Tecnologias utilizadas: laravel-zero e MySQL.

---

Após clonar o projeto 
  - é necessário instalar as dependências com `composer install`;
  - criar o database `teste_melhor_envio` no banco de dados MySQL (todas as configurações do banco de dados, como servidor, usuário, senha, porta e nome do database podem ser alteradas no arquivo `app/config/database.php`, no índice `connections` -> `mysql`);
  - e executar `php teste-melhor-envio migrate`, para que o laravel execute as migrações do banco para criar as tabelas.

---
## Comandos disponíveis
`log:import {caminho do arquivo}`: Importa o arquivo de logs para o banco de dados, é obrigatório informar o caminho do arquivo para leitura.

`report:average {caminho de saída do arquivo CSV (opcional)}`: Exporta o relatório de média de tempo de resposta por serviço; é possível informar o caminho completo para o arquivo de saída, caso não seja informado, será salvo na raiz do projeto /export/average.csv.

`report:consumer {caminho de saída do arquivo CSV (opcional)}`: Exporta o relatório de requests por consumidor; é possível informar o caminho completo para o arquivo de saída, caso não seja informado, será salvo na raiz do projeto /export/consumer.csv.

`report:service {caminho de saída do arquivo CSV (opcional)}`: Exporta o relatório de requests por serviço; é possível informar o caminho completo para o arquivo de saída, caso não seja informado, será salvo na raiz do projeto /export/service.csv.

A execução de todos os comandos deverá ser a partir da pasta raiz do projeto com o prefixo `php teste-melhor-envio` e o comando desejado (`log:import ./assets/logs.txt`, por exemplo).

## Definição do banco de dados
O banco de dados conta com uma única tabela: `logs` 

Todos os dados são salvos diretamente nesta tabela para que seja otimizada a velocidade de importação dos dados de logs e a exportação dos relatórios

Seria possível utilizar o isolamento de cada entidade criando uma tabela para logs, outra para consumidores e outra para serviços, porém o tempo de processamento da importação dos dados, principalmente, seria extremamente afetado, o que não é o desejado nesta aplicação de leitura de logs.

Os campos `service_name`, `consumer_uuid` e `consumer_ip` são indexados nesta tabela para contribuir com a performance na consulta dos relatórios para exportação.

## Testes

É fundamental que para executar os testes corretamente o banco de dados esteja sem registros, apenas com a estrutura da tabela criada pelo comando `php teste-melhor-envio migrate`. Os casos que necessitam dos dados importados, automaticamente executam o comando `log:import`.

Os testes que necessitam dos dados importados são executados com transação, para que ao final seja efetuado o rollback e não fiquem dados para o próximo teste. Estes testes também necessitam que o arquivo de logs esteja em `assets/logs.txt` na raiz do projeto.

Existem testes para cada tipo de relatório verificando se o processo todo ocorreu como o esperado, se houve falha de permissão ao salvar o arquivo de saída e se os relatórios foram executados sem registros no banco de dados.

Também existem testes unitários para os adapters e para o exportador CSV.

## Requisitos não cumpridos
- Docker